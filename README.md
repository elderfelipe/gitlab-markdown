> Welcome to GitLab and your first repository experience!  
>
> Rememeber, code is the language of today's innovation, and _git repositries_ offer the underlying "filesystem"
> for all code written by human beings. Speaking the language of _git_, hence, is a requirement for anyone:
> - writing or modifying code
> - handing over or managing code, without understanding of how it works
> - looking for information in code, finding bugs and knowing when they occured
> - analysing contributions (e.g. to determine legal ownership of the code)
> - using and running the code for personal and commercial purposes
>
> A you can see, not only coders need to know and understand git, so let's start using GitLab by creating a simple
> Markdown file in this repository ⭐

# Contribution Guidlines

_Contribution guidelines_ are important rules to be followed when contributing to a shared repository. Like this one.

In this repository, we only have a single guideline. This is for the naming of files created in the repo. This is the filenmaing policy below.

## Filenaming policy

You must name files in the following way:  
    `marticultion number`-`first`_`last`.md

A description of the placeholders is as follows:
- `marticulation number`: is the number that appears on your student card, which you use for the @classroom login
- `first`: is your first (or given) name. This is the name that you do not share with other members of the family.
- `last`: is your last (or family) name. This is the name that you share with other members of the family.

### Details

- If `first` or `last` are compound names (i.e. has two or more words), choose the one word that represents you best.
- If `first` or `last` contain non latin characters (like the umlaute letters in German ä, ö, or ü), replace these with latin-letter equivalents (i.e. "ae", "oe", or "ue"). If not possible, substitute them with a latin-letter parallel (e.g. Ç → C)
